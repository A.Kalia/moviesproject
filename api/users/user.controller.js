const {create, getUserByUserEmail, getMovies, getRating, updateMovie} = require('./user.service')
const {genSaltSync, hashSync, compareSync} = require("bcrypt")
const { sign } = require("jsonwebtoken")
const jsonwebtoken = require('jsonwebtoken')

module.exports = {
    createUser: (req, res) => {
        const body = req.body
        const salt = genSaltSync(10)
        body.password = hashSync(body.password, salt)
        create(body, (err, results) => {
            if(err){
                console.log(err)
                return res.status(500).json({
                    success: 0,
                    message: "Database connection error"
                })
            }
            return res.status(200).json({
                success: 1,
                data: results
            })
        })
    },
    login: (req, res) => {
        const body = req.body
        getUserByUserEmail(body.email, (err, results)=> {
            if(err){
                console.log(err)
            }
            if(!results){
              return res.json({
                  success: 0,
                  data: "Invalid email or password"
              })
            }
            const result = compareSync(body.password, results.password)
            if(result){
                results.password = undefined
                const jsontoken = sign({result: results}, "qwe1234", {
                    expiresIn: "1h"
                })
                return res.json({
                   success: 1,
                   message: "Login successfully",
                   token: jsontoken
                })
            } else {
                return res.json({
                    success: 0,
                    message: "Invalid email or password",
                 })
            }
        })
    },
    getMovies: (req, res) => {
        getMovies((err, results) => {
          if (err) {
            console.log(err);
            return;
          }
          return res.json({
            success: 1,
            data: results
          });
        });
    },
    getRating: (req, res) => {
        getRating((err, results) => {
          if (err) {
            console.log(err);
            return;
          }
          return res.json({
            success: 1,
            data: results
          });
        });
      },
      updateMovie: (req, res) => {
        const body = req.body;
        updateUser(body, (err, results) => {
          if (err) {
            console.log(err);
            return;
          }
          return res.json({
            success: 1,
            message: "updated successfully"
          });
        });
      },
}