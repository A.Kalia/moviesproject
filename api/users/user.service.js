const pool = require("../../config/database")

module.exports = {
    create: (data, callBack) => {
        pool.query(
            `insert into registration(firstName, lastName, gender, email, password, number)
                values(?,?,?,?,?,?)`,
            [
                data.first_name,
                data.last_name,
                data.gender,
                data.email,
                data.password,
                data.number
            ],
            (error, results, fields) => {
                if(error){
                 return callBack(error)
                }
                return callBack(null, results)
            }
        )
    },
    getUserByUserEmail: (email, callBack) => {
        pool.query(
          `select * from registration where email = ?`,
          [email],
          (error, results, fields) => {
            if (error) {
              callBack(error);
            }
            return callBack(null, results[0]);
          }
        );
      },
    getMovies: callBack =>{
        pool.query(
            `select title, vote_average, vote_count from movies`,
            [],
            (error, results, fields) => {
                if(error){
                    return callBack(error);
                }
                return callBack(null, results);
            }
        )
    },
    getRating: callBack =>{
        pool.query(
            `select title, vote_average from movies`,
            [],
            (error, results, fields) => {
                if(error){
                    return callBack(error);
                }
                return callBack(null, results);
            }
        )
    },
    updateMovie: (data, callBack) => {
        pool.query(
            `update movies set vote_count = vote_count + 1, vote_average = ((vote_average*vote_count)+?)/vote_count+1
            where id = ?`,
            [
                data.rating,
                data.id
            ],
            (error, results, fields) => {
                if(error){
                    return callBack(error);
                }
                return callBack(null, results[0]);
            }

        )
    }
}