const { createUser, login, getMovies, getRating, updateMovie } = require('./user.controller')
const router = require("express").Router()
const { checkToken } = require("../../auth/token_validation")

router.post("/", checkToken, createUser)
router.post("/login", login)
router.get("/movies", checkToken, getMovies)
router.get("/rating", getRating)
//router.patch("/", checkToken, updateMovie)
module.exports = router;
