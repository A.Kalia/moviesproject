const httpMocks = require('node-mocks-http')
const UsersCtrl = require('../api/users/user.controller')

describe('testing createUser controller', () => {
    test('for success', async () => {
        expect.assertions(2)
        // Assertion 1: request.body with no params but with token
        let req = httpMocks.createRequest({
            method: 'POST',
            body: {
                token
            },
        })
        let res = httpMocks.createResponse()
        await UsersCtrl.createUser(req, res)
        let data = res._getJSONData()
        expect(data.status).toEqual(0)
    })
    test('for success', async () => {
        // Assertion 2: request.body with all params
        let req = httpMocks.createRequest({
            method: 'POST',
            body: {
                first_name,
                last_name,
                gender,
                email,
                password,
                number,
                token
            },
        })
        let res = httpMocks.createResponse()
        await UsersCtrl.createUser(req, res)
        let data = res._getJSONData()
        expect(data.status).toEqual(0)
    })
    test('for failure', async () => {
        expect.assertions(1)
        // Assertion 1: req.body with missing mandatory param token
        const req = httpMocks.createRequest({
            method: 'POST',
            body: {
                first_name,
                last_name,
                gender,
                email,
                password,
                number
            },
        })
        const res = httpMocks.createResponse()
        await UsersCtrl.createUser(req, res)
        const data = res._getJSONData()
        expect(data.status).toEqual(-1)
    })
})

describe('testing login controller', () => {
    test('for success', async () => {
        expect.assertions(1)
        // Assertion 1: req.body with all mandatory params
        const req = httpMocks.createRequest({
            method: 'POST',
            body: {
                email,
                password
            },
        })
        const res = httpMocks.createResponse()
        await earningRateCtrl.insertEarningRate(req, res)
        const data = res._getJSONData()
        expect(data.status).toEqual(0)
    })
    test('for failure', async () => {
        expect.assertions(1)
        // Assertion 1: req.body with missing mandatory params
        const req = httpMocks.createRequest({
            method: 'POST',
            body: {},
        })
        const res = httpMocks.createResponse()
        await UsersCtrl.login(req, res)
        const data = res._getJSONData()
        expect(data.status).toEqual(-1)
    })
})

describe('testing getMovies controller', () => {
    test('for success', async () => {
        expect.assertions(1)
        // Assertion 1: req.body with all mandatory params
        const req = httpMocks.createRequest({
            method: 'GET',
            body: {
                token
            },
        })
        const res = httpMocks.createResponse()
        await UsersCtrl.getMovies(req, res)
        const data = res._getJSONData()
        expect(data.status).toEqual(0)
    })
    test('for failure', async () => {
        expect.assertions(1)
        // Assertion 1: req.body with missing mandatory params
        const req = httpMocks.createRequest({
            method: 'GET',
            body: {},
        })
        const res = httpMocks.createResponse()
        await UsersCtrl.getMovies(req, res)
        const data = res._getJSONData()
        expect(data.status).toEqual(-1)
    })
})

describe('testing getRating controller', () => {
    test('for success', async () => {
        expect.assertions(1)
        // Assertion 1: req.body with all no params
        const req = httpMocks.createRequest({
            method: 'GET',
            body: {},
        })
        const res = httpMocks.createResponse()
        await UsersCtrl.getRating(req, res)
        const data = res._getJSONData()
        expect(data.status).toEqual(0)
    })
})

